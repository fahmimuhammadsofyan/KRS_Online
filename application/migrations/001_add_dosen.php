<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_dosen extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(
                        array(
                                'nik' => array(
                                        'type' => 'INT',
                                        'constraint' => 8,
                                        'unsigned' => TRUE,
                                        'auto_increment' => TRUE
                                ),
                                'nama' => array(
                                        'type' => 'VARCHAR',
                                        'constraint' => '30',
                                ),
                                'email' => array(
                                        'type' => 'VARCHAR',
                                        'constraint' => '30',
                                        'null' => FALSE,
                                ),
                                'no_tlp' => array(
                                        'type' => 'INT',
                                        'constraint' => '12',
                                        'null' => FALSE,
                                ),
								'alamat' => array(
										'type' => 'VARCHAR',
										'constraint' => '30',
										'null' => FALSE,
								),
                        ));
                $this->dbforge->add_key('nik', TRUE);
                $this->dbforge->create_table('tbl_dosen');
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_dosen');
        }
}