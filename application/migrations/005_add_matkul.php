<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_matkul extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(
                        array(
                                'kode_matkul' => array(
                                        'type' => 'INT',
                                        'constraint' => 15,
                                        'unsigned' => TRUE,
                                        'auto_increment' => TRUE
                                ),
                                'nik' => array(
                                        'type' => 'INT',
                                        'constraint' => '8',
                                        'null' => FALSE,
                                ),
                                'nama' => array(
                                        'type' => 'VARCHAR',
                                        'constraint' => '30',
                                        'null' => FALSE,
                                ),
                        ));
                $this->dbforge->add_key('kode_matkul', TRUE);
                $this->dbforge->create_table('tbl_matkul');
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_matkul');
        }
}