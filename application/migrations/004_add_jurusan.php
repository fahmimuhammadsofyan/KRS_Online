<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_jurusan extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(
                        array(
                                'kode_jrsn' => array(
                                        'type' => 'INT',
                                        'constraint' => 15,
                                        'unsigned' => TRUE,
                                        'auto_increment' => TRUE
                                ),
                                'prodi' => array(
                                        'type' => 'VARCHAR',
                                        'constraint' => '30',
                                ),
                                'kode_matkul' => array(
                                        'type' => 'INT',
                                        'constraint' => '15',
                                        'null' => FALSE,
                                ),
                         
                        ));
                $this->dbforge->add_key('kode_jrsn', TRUE);
                $this->dbforge->create_table('tbl_jurusan');
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_jurusan');
        }
}