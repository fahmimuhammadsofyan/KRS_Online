<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_jadwal extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(
                        array(
                                'kode_jdwl' => array(
                                        'type' => 'INT',
                                        'constraint' => 15,
                                        'unsigned' => TRUE,
                                        'auto_increment' => TRUE
                                ),
                                'kode_matkul' => array(
                                        'type' => 'INT',
                                        'constraint' => '15',
                                ),
                                'nrp' => array(
                                        'type' => 'INT',
                                        'constraint' => '9',
                                        'null' => FALSE,
                                ),
                                'nik' => array(
                                        'type' => 'INT',
                                        'constraint' => '8',
                                        'null' => FALSE,
                                ),
                        ));
                $this->dbforge->add_key('kode_jdwl', TRUE);
                $this->dbforge->create_table('tbl_jadwal');
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_jadwal');
        }
}