<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_semester extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(
                        array(
                                'kode_jrsn' => array(
                                        'type' => 'INT',
                                        'constraint' => 15,
                                        'unsigned' => TRUE,
                                        'auto_increment' => TRUE
                                ),
                                'nilai' => array(
                                        'type' => 'INT',
                                        'constraint' => '5',
                                ),
                                'id' => array(
                                        'type' => 'INT',
                                        'constraint' => '7',
                                        'null' => FALSE,
                                ),
                        ));
                $this->dbforge->add_key('kode_jrsn', TRUE);
                $this->dbforge->create_table('tbl_semester');
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_semester');
        }
}